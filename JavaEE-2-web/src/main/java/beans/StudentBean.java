package beans;

import lombok.Data;
import model.Student;
import org.jboss.weld.context.ejb.Ejb;
import repository.StudentRepository;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@Data
@ManagedBean
@SessionScoped
public class StudentBean implements Serializable {
    private List<Student> studentList = new ArrayList<>();
    @Ejb
    private Student student;
    private boolean edit;
    @Inject
    private StudentRepository studentRepository;
    @PostConstruct
    public void init(){
        getAllStudents();
    }

    public void add(Student student){
        studentRepository.add(student);
    }
    public void update(Student student){
       Student std= studentRepository.update(student);
    }
    public void delete(int id){
        studentRepository.delete(id);
    }
    public void getAllStudents(){
        this.studentList = studentRepository.findAll();
    }

    public void onFacultyChanged(){
        System.out.println("faculty changed");
        String faculty = student.getFaculty();
        this.student.setFaculty(faculty);

    }

}
