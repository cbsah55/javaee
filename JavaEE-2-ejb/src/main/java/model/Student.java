package model;

import javax.annotation.ManagedBean;
import javax.persistence.*;
import java.io.Serializable;

@ManagedBean
@Entity
@NamedQuery(name = "student.findAll", query = "select s from  Student s ")
@Table(name = "student")
public class Student implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String faculty;
    private String programme;
}
