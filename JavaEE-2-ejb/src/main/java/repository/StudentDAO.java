package repository;

import model.Student;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Stateless
public class StudentDAO implements StudentRepository {
    @PersistenceContext
    private EntityManager em;
    @Transactional
    @Override
    public void add(Student student) {
        em.persist(student);

    }
@Transactional
    @Override
    public Student update(Student student) {

        return em.merge(student);
    }
    @Transactional
    @Override
    public void delete(int id) {
        em.remove(findOne(id));
    }
    @Transactional
    @Override
    public Student findOne(int id) {
        return em.find(Student.class,id);
    }
    @Transactional
    @Override
    public List<Student> findAll() {
        return em.createNamedQuery("student.findAll",Student.class).getResultList();
    }
}
