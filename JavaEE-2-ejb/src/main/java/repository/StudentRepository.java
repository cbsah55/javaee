package repository;

import model.Student;

import java.util.List;

public interface StudentRepository {
    void add(Student student);
    Student update(Student student);
    void delete(int id);
    Student findOne(int id);
    List<Student> findAll();
}
